import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.createForm();
  }

  loginForm: FormGroup;
  erro;

  ngOnInit() {

  }

  createForm() {
    this.loginForm = this.fb.group({

      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],

      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])]

    });
  }

  loginSubmit(data) {

    this.http.post('http://localhost:8000/api/login', data).subscribe(
      result => {

        const obj = Object.keys(result).map(key => result[key]);

        window.localStorage['token'] = obj[0].token;

        this.router.navigate(['places']);

      },
      err => {
        alert('Dados Inválidos');

        this.loginForm.reset();
      }
    );
  }

}
