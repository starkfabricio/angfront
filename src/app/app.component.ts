import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
      private router: Router
  ) {}

  logout() {

    window.localStorage.removeItem('jwtToken');

    this.router.navigate(['']);

  }

  logged() {
    if (window.localStorage['token'] != null) {
      return true;
    } else {
      return false;
    }
  }
}
