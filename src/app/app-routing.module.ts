import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PlaceComponent } from './place/place.component';
import { PlaceShowComponent } from './place/place-show/place-show.component';
import { PlaceFormsComponent } from './place/place-forms/place-forms.component';

const appRoutes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'places', component: PlaceComponent },
  { path: 'places/:id', component: PlaceShowComponent },
  { path: 'places/:id/edit', component: PlaceFormsComponent },
  { path: 'places/create/new', component: PlaceFormsComponent },
  /*  { path: 'users',
      loadChildren: 'app/users/users.module#UsersModule',
    },*/
  //  { path: '**', component: Page404Component } //, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
