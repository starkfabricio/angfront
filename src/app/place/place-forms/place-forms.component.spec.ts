import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceFormsComponent } from './place-forms.component';

describe('PlaceFormsComponent', () => {
  let component: PlaceFormsComponent;
  let fixture: ComponentFixture<PlaceFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
