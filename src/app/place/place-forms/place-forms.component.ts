import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-place-forms',
  templateUrl: './place-forms.component.html',
  styleUrls: ['./place-forms.component.scss']
})
export class PlaceFormsComponent implements OnInit {

  place;
  placeForm: FormGroup;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {

    this.place = this.route.params
      .subscribe(
        params => {

          this.http.get('http://localhost:8000/api/places/' + params['id'])
            .subscribe(place => {

              this.place = Array.of(place);
              this.place = this.place[0];

              this.placeForm.patchValue({
                name: this.place.name,
                code: this.place.code,
                type: this.place.type,
                price: this.place.price,
                area: this.place.area,
                description: this.place.description
              });

            });
        }
      );

    this.createForm();

  }

  createForm() {
    this.placeForm = this.fb.group({

      name: ['', Validators.compose([
        Validators.required,
      ])],

      code: ['', Validators.compose([
        Validators.required,
      ])],

      type: ['', Validators.compose([
        Validators.required,
      ])],

      price: ['', Validators.compose([
        Validators.required,
      ])],

      area: ['', Validators.compose([
        Validators.required,
      ])],

      description: ['', Validators.compose([
        Validators.required,
      ])],

    });
  }

  placeSubmit(data) {

    // Edit
    if (this.route.snapshot.url[2].path === 'edit') {

      this.place = this.route.params
      .subscribe(
        params => {

          this.http.patch('http://localhost:8000/api/places/' + params['id'], data).subscribe(
        result => {
          alert('Imóvel Atualizado com Sucesso!');
          this.router.navigate(['places']);
        }
      );
        }
      );

    } else {

      // Create
      this.http.post('http://localhost:8000/api/places', data).subscribe(
        result => {
          alert('Imóvel Criado com Sucesso!');
          this.router.navigate(['places']);
        }
      );
    }

  }

  exists(data) {

    if (data) {
      return data;
    } else {
      return '';
    }

  }

}
