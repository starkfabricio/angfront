import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {

  searchForm: FormGroup;

  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FormBuilder
  ) { }
  places;
  // places
  ngOnInit() {
    this.getPlaces(1);
    this.createForm();
  }

  getPlaces(page) {
    this.http.get('http://localhost:8000/api/places?page=' + page)
      .subscribe(
        places => {
          this.places = Object.values(places);
          this.places = this.places[0];
        }
      );
  }

  show(id) {
    this.router.navigate(['places', id]);
  }

  delete(id) {

    this.http.delete('http://localhost:8000/api/places/' + id).subscribe(
      response => {
        this.getPlaces(1);
      }
    );

  }

  createForm() {
    this.searchForm = this.fb.group({

      code: ['', Validators.compose([
        Validators.required,
      ])]

    });
  }

  searchSubmit(id) {

    this.router.navigate(['places', id.code]);

  }

}
