import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-place-show',
  templateUrl: './place-show.component.html',
  styleUrls: ['./place-show.component.scss']
})
export class PlaceShowComponent implements OnInit {

  place;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.place = this.route.params
    .subscribe(
      params => {
        this.http.get('http://localhost:8000/api/places/' + params['id'])
        .subscribe(place => {

          this.place = Array.of(place);
          this.place = this.place[0];

        });
      }
    );

  }

}
